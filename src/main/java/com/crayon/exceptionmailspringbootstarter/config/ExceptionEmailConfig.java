package com.crayon.exceptionmailspringbootstarter.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 异常邮件配置类
 * @author Yuqiang
 */
@Data
@Component
@ConfigurationProperties("yu.exception.mail")
public class ExceptionEmailConfig {


    private String from;
    private String to;
    private String password;
    private String host;

}
