package com.crayon.exceptionmailspringbootstarter.config;

import com.crayon.exceptionmailspringbootstarter.service.MailService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 自动配置类
 * @author Yuqiang
 */
@Configuration
@ConditionalOnClass(MailService.class)
@ConditionalOnProperty(prefix = "yu.exception", name = "enable", havingValue = "true")//校验是否加载配置
@EnableConfigurationProperties({ExceptionEmailConfig.class, ExecutorConfig.class})
public class CrayonExceptionAutoConfigure {

}
