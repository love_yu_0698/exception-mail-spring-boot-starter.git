package com.crayon.exceptionmailspringbootstarter.config;


import com.crayon.exceptionmailspringbootstarter.resolver.CustomizeExceptionHandler;
import com.crayon.exceptionmailspringbootstarter.service.MailService;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;

import java.util.concurrent.*;


/**
 * 线程池配置类
 * @author Yuqiang
 */
@Data
@Component
@EnableAsync
@ConfigurationProperties("yu.exception.threadpool")
public class ExecutorConfig {

    private Integer coresize;
    private Integer maxpoolsize;
    private Integer alivetime;
    private Integer blockingsize;

    @Bean
    public Executor threadPoolExecutor() {

        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(blockingsize);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(coresize, maxpoolsize, alivetime, TimeUnit.MINUTES, workQueue);
        threadPoolExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
        return threadPoolExecutor;
    }

    @Bean
    public MailService mailService() {

        return new MailService();
    }

    @Bean
    public HandlerExceptionResolver exceptionResolver() {

        return new CustomizeExceptionHandler();
    }
}
