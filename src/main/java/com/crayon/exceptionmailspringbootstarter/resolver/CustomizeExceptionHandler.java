package com.crayon.exceptionmailspringbootstarter.resolver;


import com.crayon.exceptionmailspringbootstarter.service.MailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Yuqiang
 */
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CustomizeExceptionHandler implements HandlerExceptionResolver {

	@Resource
	private MailService mailService;

	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
                                         Exception ex) {
		//发送邮件
		System.out.println("======================进入CustomizeExceptionHandler  异常邮件");
		mailService.sendExceptionEmail(ex, request);
        return null;
	}
}
