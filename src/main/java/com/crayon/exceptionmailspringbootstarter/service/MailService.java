package com.crayon.exceptionmailspringbootstarter.service;

import com.crayon.exceptionmailspringbootstarter.config.ExceptionEmailConfig;

import com.sun.mail.util.MailSSLSocketFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.util.Properties;

/**
 * 邮件服务
 * @author Yuqiang
 */
@Component
public class MailService {

    @Resource
    private ExceptionEmailConfig exceptionEmailConfig;


    public void sendExceptionEmail(Exception a, HttpServletRequest request) {
        String uri;
        String to = exceptionEmailConfig.getTo();
        String from = exceptionEmailConfig.getFrom();
        String password = exceptionEmailConfig.getPassword();
        String hostName = "";
        String ip = "";
        String errorMsg;
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);

        try {
            InetAddress localHost = InetAddress.getLocalHost();
            hostName = localHost.getHostName();
            ip = localHost.getHostAddress();
        } catch (UnknownHostException hostException) {
            hostException.printStackTrace();
        }

        uri = request.getRequestURI();
        a.printStackTrace(pw);
        errorMsg = sw.toString();


        //跟smtp服务器建立一个连接
        Properties p = new Properties();
        // 设置邮件服务器主机名
        p.setProperty("mail.host", "smtp.qq.com");//指定邮件服务器
        // 发送服务器需要身份验证
        p.setProperty("mail.smtp.auth", "true");//要采用指定用户名密码的方式去认证
        // 发送邮件协议名称
        p.setProperty("mail.transport.protocol", "smtp");
        // 设置端口 默认端口 25
        p.setProperty("mail.smtp.port", "465");

        // 开启SSL加密，否则会失败
        MailSSLSocketFactory sf = null;
        try {
            sf = new MailSSLSocketFactory();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        sf.setTrustAllHosts(true);
        p.put("mail.smtp.ssl.enable", "true");
        p.put("mail.smtp.ssl.socketFactory", sf);

        // 创建session
        Session session = Session.getDefaultInstance(p, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                //用户名可以用QQ账号也可以用邮箱的别名
                PasswordAuthentication pa = new PasswordAuthentication(from, password);
                // 后面的字符是授权码，用qq密码不行！！
                return pa;
            }
        });

        try {
            //声明一个Message对象(代表一封邮件),从session中创建
            MimeMessage msg = new MimeMessage(session);
            //邮件信息封装
            //1发件人
            msg.setFrom(new InternetAddress(from));
            //2收件人
            msg.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(to));
            //3邮件内容:主题、内容
            msg.setSubject("异常邮件");
            msg.setContent(getContext(hostName, ip, uri, errorMsg), "text/plain;charset=utf-8");//纯文本
            //发送动作
            Transport.send(msg);
        } catch (MessagingException e) {
            e.printStackTrace();
        }


    }


    private String getContext(String hostName, String ip, String uri, String errorMsg) {

        return "服务所在主机名：" +
                hostName +
                "\n" +
                "IP：" +
                ip +
                "\n" +
                "调用的url：" + uri +
                "\n" +
                "异常详细信息：" +
                "\n" +
                errorMsg;
    }
}
